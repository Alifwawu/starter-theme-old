<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package cones
 */

?>
<!doctype html>
<html <?php language_attributes(); ?> id="html">
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="https://gmpg.org/xfn/11">

	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
<div id="page" class="site">

<?php (is_front_page()) ? $hh = 'home-header' : $hh = ''; ?>

			<header id="masthead" class="fixed-top site-header <?php echo $hh; ?>">

			<div class="container">
				<div class="row">


					<div class="logo d-flex align-items-center">
						<?php if (has_custom_logo()) {the_custom_logo();} else {?>
						<a href="<?php echo esc_url(home_url('/')); ?>" rel="home"><?php bloginfo('name');?></a>
						<?php }?>
					</div><!-- .logo -->

					<nav id="site-navigation" class="main-navigation">
						<?php
						wp_nav_menu(array(
							'theme_location' => 'primary',
							'depth' => 2,
							'container' => false,
							'container_class' => 'collapse navbar-collapse',
							'container_id' => 'bs-example-navbar-collapse-1',
							'menu_class' => 'nav',
							'fallback_cb' => 'WP_Bootstrap_Navwalker::fallback',
							'walker' => new Bootstrap_Walker_Nav_Menu(),
						));
						?>
					</nav><!-- #site-navigation -->

					<div class="menu-buttons ml-auto">
						<button class="hamburger hamburger--collapse" type="button">
							<span class="hamburger-box">
								<span class="hamburger-inner"></span>
							</span>
						</button>
					</div>

				</div><!-- /.row -->
			</div><!-- /.container -->

        </header><!-- #masthead -->	
	<?php
		$templates = array(
		'template-homepage.php'
	);

	(is_page_template($templates) || is_home() || is_single() || is_404() || is_archive() || is_404()) ? $container = 'container-fluid' : $container = 'container';
	?>

	<div id="content" class="site-content">
		<div class="<?php echo $container; ?>">
			<div class="row">