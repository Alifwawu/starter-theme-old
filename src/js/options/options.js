/**
 * Hero slider options
 */
export const heroSliderOptions = {
	speed: 500,
	loop: true,
	loopedSlides: 5,
	draggable: true,
	centeredSlides: true,
	slidesPerView: 1,
	//initialSlide: 1, //3
	pagination: {
		el: ".hero-slider--pagination",
		type: "bullets",
		clickable: true
	},
	// Navigation arrows
	navigation: {
		nextEl: ".hero-slider--next",
		prevEl: ".hero-slider--prev"
	}
};
