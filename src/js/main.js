import {
	swiper,
	headerAddClass,
	hamburger,
} from "./functions/functions";

import { heroSliderOptions } from "./options/options";

(function ($) {
	//console.log(window);

	/**
	 * Make sliders
	 * @docs https://swiperjs.com/get-started/
	 * @src ./options/options.js
	 * @src ./functions/functions.js
	 */
	swiper("#hero-slider .swiper-container", heroSliderOptions); // Hero slider

	/**
	 * Add class on scroll
	 * @src ./functions/functions.js
	 */
	headerAddClass('.site-header', 'header-scrolled', 300);

	hamburger('.hamburger');

	/**
	 *  Back to top animation
	 */
	$('#back-to-top').click(function (e) {

		e.preventDefault();

		$('html,body').animate({ scrollTop: 0 }, 300);

	});

})(jQuery);
