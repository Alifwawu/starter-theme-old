import Swiper from 'swiper';

/**
 * Check if element is in dom
 * @param element @type string - element to check
 */
const isElementInDOM = element => typeof element != undefined && element != null;

/**
 * Swipe the element
 * @param element @type string - element to assign slider to
 * @param options @type object - options for slider - https://swiperjs.com/api/
 */
export let swiper = (element, options) => {
    const el = document.querySelector(element);
    if (isElementInDOM(el))
        new Swiper(el, options);
}


/**
 * Add class on scoll
 * @param element @type string - element to add class to
 * @param classToAdd @type string - class to add to element
 * @param fromTop @type int - how many pixels from top
 */
export let headerAddClass = (element, classToAdd, fromTop) => {
    const el = document.querySelector(element);

    const callback = () => {
        const y = window.pageYOffset;
        if (y >= fromTop)
            el.classList.add(classToAdd)
        else
            el.classList.remove(classToAdd)
    }

    window.addEventListener('scroll', callback);
}

/**
 * Hamburger
 */
export let hamburger = (element) => {
    const el = document.querySelector(element);
    const siteNav = document.querySelector('#site-navigation');
    const html = document.querySelector('#html');

    if (isElementInDOM(el))
        el.addEventListener('click', function () {
            el.classList.toggle('is-active');
            siteNav.classList.toggle('is-active');
            html.classList.toggle('is-active');
        });
}